<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LandlordController;
use App\Http\Controllers\LocataireController;

use App\Http\Controllers\ImmeubleController;
use App\Http\Controllers\LocationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('dashboard', [CustomAuthController::class, 'dashboard'])->name('dashboard')->middleware('auth'); 
Route::get('login', [CustomAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CustomAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CustomAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

Route::get('/',[HomeController::class,'index'])->name('home.index');
Route::get('/show/{id}',[HomeController::class,'show'])->name('home.show');
// Route::get('/a', function () {
//     return view('layoutsHome.app');
// });



Route::resource('landlord', LandlordController::class);
Route::resource('locataires', LocataireController::class);

Route::resource('locations', LocationController::class);

Route::resource('immeubles', ImmeubleController::class);

