<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('immeubles', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('type_location');
            $table->string('addresse');
            $table->string('telephone');
            $table->text('images')->nullable();
            $table->string('ville');
            $table->string('code');
            $table->string('region');
            $table->string('surface');
            $table->string('etage');
            $table->float('prix');
            $table->string('description');
            $table->float('nbr_chambre');   
            $table->string('info'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('immeubles');
    }
};
