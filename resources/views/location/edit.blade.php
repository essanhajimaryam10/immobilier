@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
 location
@endsection

@section('title_page2')
@endsection

@section('content')

<div class="card mb-4">
   @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif 
  <div class="card-header">
  Edit  location
  </div>
  <div class="card-body">
  <form method="POST" action="{{ route('locations.update',$location->id) }}" enctype="multipart/form-data">
      @csrf 
@method('PUT')

      <div class="row">
        <div class="col">
          <div class="mb-3 row">
          <label for="immeuble" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Immeuble</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="immeuble_id"  name="immeuble_id">
                    @foreach($immeubles as $immeuble)
                    <option value="{{$immeuble->id}} ">{{$immeuble->type}}</option>
                    @endforeach
                </select>
     </div>
   </div>
   <div class="col">
          <div class="mb-3 row">
          <label for="type_location" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type de location</label> <br>
              <select class="col-lg-10 col-md-6 col-sm-12" id="type_location"  value='{{$location->type_location}}' name="type_location">
                <option value="habitationvide"   {{$location->type_location == 'habitationvide' ? 'selected' : '' }}>Bail d'habitation vide</option>
                <option value="habitationmeble"  {{$location->type_location == 'habitationmeble' ? 'selected' : '' }}>Bail d'habitation meublé</option>
                <option value="etudiant"  {{$location->type_location == 'etudiant' ? 'selected' : '' }}>Bail meublé etudiant</option>
                <option value="parkig"  {{$location->type_location == 'parkig' ? 'selected' : '' }}>Bail parking/garage</option>
                <option value="mixte"  {{$location->type_location == 'mixte' ? 'selected' : '' }}>Bail mixte</option>
                <option value="commercial"  {{$location->type_location == 'commercial' ? 'selected' : '' }}>Bail commercial</option>
                <option value="autre"  {{$location->type_location == 'autre' ? 'selected' : '' }}>Autre</option>
              </select>
  </div>
    </div>
    </div>

<div class="row">

    
<div class="col">
          <div class="mb-3 row">
            <label for="locataire" class="col-lg-2 col-md-6 col-sm-12 col-form-label">locataire</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="locataire_id"  name="locataire_id">
                    @foreach($locataires as $locataire)
                    <option value="{{$locataire->id}}">{{$locataire->type_locataire}}</option>
                    @endforeach
                </select>
     </div>
   </div>
 
 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Debut du bail</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="debut_bail" value='{{$location->debut_bail}}' type="date" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Fin  du bail</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="fin_bail" value='{{$location->fin_bail}}' type="date" class="form-control">
            </div>
          </div>
        </div>
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Moyen de payement</label>
            <div class="col-lg-10 col-md-6 col-sm-12">

              <select class="col-lg-10 col-md-6 col-sm-12" value='{{$location->moyen_payement}}' id="moyen_payement"  name="moyen_payement">
                    <option value="bureaux" {{$location->moyen_payement == 'bureaux' ? 'selected' : '' }}>Bureaux</option>
                    <option value="carte" {{$location->moyen_payement == 'carte' ? 'selected' : '' }} >Carte de crédit</option>
                    <option value="cheque" {{$location->moyen_payement == 'cheque' ? 'selected' : '' }}> Chéque</option>
                    <option value="especes" {{$location->moyen_payement == 'especes' ? 'selected' : '' }}> Espéces</option>
                    <option value="virement" {{$location->moyen_payement == 'virement' ? 'selected' : '' }}> Virement</option>

                </select>
            </div>
          </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Payement</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
        
                <select class="col-lg-10 col-md-6 col-sm-12" id="payement"  value='{{$location->payement}}' name="payement">
                    <option value="mensuel"  {{$location->payement == 'mensuel' ? 'selected' : '' }} >Mensuel</option>
                    <option value="bimestriel" {{$location->payement == 'bimestriel' ? 'selected' : '' }} >Bimestriel</option>
                    <option value="trimestriel" {{$location->payement == 'trimestriel' ? 'selected' : '' }} >Trimestriel</option>
                    <option value="quadrimestriel" {{$location->payement == 'quadrimestriel' ? 'selected' : '' }} >Quadrimestriel</option>
                    <option value="semestriel" {{$location->payement == 'semestriel' ? 'selected' : '' }} >Semestriel</option>
                    <option value="annuel" {{$location->payement == 'annuel' ? 'selected' : '' }} >Annuel</option>
                    <option value="forfaitaire" {{$location->payement == 'forfaitaire' ? 'selected' : '' }} >Forfaitaire</option>
                </select>
              </div>
            </div>
          </div> 

        </div> 
        <a class="btn btn-primary" href="{{ route('locations.index') }}"> Retour</a>
  
        <button type="submit" class="btn btn-primary">Ajouter</button>
</div>
</div>
@endsection