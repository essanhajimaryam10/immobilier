@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
 location
@endsection

@section('title_page2')
@endsection

@section('content')

<div class="card mb-4">
   @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif 
  <div class="card-header">
  Creer nouveau location
  </div>
  <div class="card-body">
  <form method="POST" action="{{ route('locations.store') }}" enctype="multipart/form-data">
      @csrf

      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label for="immeuble" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Immeuble</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="immeuble_id"  name="immeuble_id">
                  <option value="">Selecte type d'immeuble</option>

                    @foreach($immeubles as $immeuble)
                    <option value="{{$immeuble->id}}">{{$immeuble->type}}</option>
                    @endforeach
                </select>
     </div>
   </div>
   <div class="col">
          <div class="mb-3 row">
          <label for="type_location" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type de location</label> <br>
              <select class="col-lg-10 col-md-6 col-sm-12" id="type_location"  name="type_location">
              <option value="">Selecte type  de location</option>

                <option value="habitationvide">Bail d'habitation vide</option>
                <option value="habitationmeble">Bail d'habitation meublé</option>
                <option value="etudiant">Bail meublé etudiant</option>
                <option value="parkig">Bail parking/garage</option>
                <option value="mixte">Bail mixte</option>
                <option value="commercial">Bail commercial</option>
                <option value="autre">Autre</option>
              </select>
  </div>
    </div>
    </div>

<div class="row">

<div class="col">
          <div class="mb-3 row">
            <label for="locataire" class="col-lg-2 col-md-6 col-sm-12 col-form-label">locataire</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="locataire_id"  name="locataire_id">
              <option value="">Selecte type  de locataire</option>

                    @foreach($locataires as $locataire)

                    <option value="{{$locataire->id}}">{{$locataire->type_locataire}}</option>
                    @endforeach
                </select>
     </div>
   </div>
 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Debut du bail</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="debut_bail" value="{{ old('debut_bail') }}" type="date" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Fin  du bail</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="fin_bail" value="{{ old('fin_bail') }}" type="date" class="form-control">
            </div>
          </div>
        </div>
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Moyen de payement</label>
            <div class="col-lg-10 col-md-6 col-sm-12">

              <select class="col-lg-10 col-md-6 col-sm-12" id="moyen_payement"  name="moyen_payement">
              <option value="">Selecte Moyen de payement</option>

                    <option value="carte">Carte de crédit</option>
                    <option value="cheque"> Chéque</option>
                    <option value="especes"> Espéces</option>
                    <option value="virement"> Virement</option>

                </select>
            </div>
          </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Payement</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
        
                <select class="col-lg-10 col-md-6 col-sm-12" id="payement"  name="payement">
              <option value="">Selecte Payement</option>
                    <option value="mensuel">Mensuel</option>
                    <option value="bimestriel">Bimestriel</option>
                    <option value="trimestriel">Trimestriel</option>
                    <option value="quadrimestriel">Quadrimestriel</option>
                    <option value="semestriel">Semestriel</option>
                    <option value="annuel">Annuel</option>
                    <option value="forfaitaire">Forfaitaire</option>
                </select>
              </div>
            </div>
          </div> 

        </div> 
        <a class="btn btn-primary" href="{{ route('locations.index') }}"> Retour</a>
  
        <button type="submit" class="btn btn-primary">Ajouter</button>
</div>
</div>
@endsection