@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
Location
@endsection

@section('title_page2')

@endsection
@section('content')
<div class="row">

</div>
<div class="dropdown-divider border-black"></div>
<div class="row ml-4 ">
    <div class="col-md-6">
        <h5 class="fw-bold mb-0">liste des Locations</h5>
    </div>
    <div class="col-md-6">
        <!-- <div class="d-flex justify-content-end">
                <a href="/process.php?action=export" class="btn btn-success btn-sm" id="export"><i class="fas fa-table"></i> Exporter</a>
            </div> -->
    </div>
</div>
<div class="dropdown-divider border-black"></div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="button">
                        <a href="{{ route('locations.create') }}" class="btn btn-sm btn-primary">nouveau location</a>
                    </div>
                    <br>
                    <form method="GET">
                        <div class="input-group mb-3">
                            <input type="text" name="search" value="" class="form-control" placeholder="recherche par payement..." aria-label="Search" aria-describedby="button-addon2">
                            <button class="btn btn-success" type="submit" id="button-addon2">Recherche</button>
                        </div>
                    </form>
                    <table class="table table-responsive-sm table-bordered table-striped">
                        <thead>
                            <th scope="col">Locataire</th>

                            <th scope="col">Immeuble</th>

                            <th scope="col">Debut du bail</th>
                            <th scope="col">Fin du bail</th>
                            <th scope="col">Paiment</th>

                            <th scope="col ">Action</th>
                        </thead>
                        <tbody>
                            @foreach ($locations as $location)

                            <tr>
                                <td>{{ $location->Locataire->type_locataire }}</td>
                                <td> {{$location->Immeuble->type}} </td>
                                <td>{{ $location->debut_bail }}</td>
                                <td>{{ $location->fin_bail}}</td>
                                <td>{{ $location->payement }}</td>
                                <td class="text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn btn-success" href="{{ route('locations.edit',$location->id) }}">modifier</a>
                                        </div>
                                        <div class="col-md-6">

                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $location->id }}">Supprimer</button>
                                        </div>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal{{ $location->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <center>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title">Supprimer la confirmation de location</h3><br>
                                                </div>
                                            </center>
                                                <div class="modal-body">
                                                    <p>Êtes-vous sûr de vouloir supprimer cet location?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-hover btn-primary" data-dismiss="modal">Annuler</button>
                                                    <form action="{{ route('locations.destroy', ['location' => $location->id]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (isset($message))
    <div class="alert alert-info">{{ $message }}</div>
@endif
        
                </div>
                <div class="card-body d-flex justify-content-left">
                    {!! $locations->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection