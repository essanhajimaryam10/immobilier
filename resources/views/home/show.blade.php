@extends('layoutsHome.app')
@extends('layouts.head')

<style>
  /*****************globals*************/
  body {
    font-family: 'open sans';
    overflow-x: hidden;
  
  }

  img {
    max-width: 100%;
  }


  @media screen and (max-width: 996px) {
    .preview {
      margin-bottom: 20px;
    }
  }

  .preview-pic {
   
    flex-grow: 1;
  }

  .preview-thumbnail.nav-tabs {
    border: none;
    margin-top: 15px;
  }

  .preview-thumbnail.nav-tabs li {
    width: 18%;
    margin-right: 2.5%;
  }

  .preview-thumbnail.nav-tabs li img {
    max-width: 100%;
    display: block;
  }

  .preview-thumbnail.nav-tabs li a {
    padding: 0;
    margin: 0;
  }

  

  .tab-content {
    overflow: hidden;
  }

  .tab-content img {
    width: 100%;
    -webkit-animation-name: opacity;
    animation-name: opacity;
    -webkit-animation-duration: .3s;
    animation-duration: .3s;
  }

  .card {
    margin-top: 50px;
    background: #eee;
    padding: 3em;
    line-height: 1.5em;
  }





  .checked,
  .price span {
    color: #3554d1;
  }

  .product-title {
    margin-top: 0;
  }


</style>
@section('content1')

<div class="container">
  <div class="card">
    <div class="container-fliud">
      <div class="wrapper row">
        <div class="preview col-md-6">

          <div class="preview-pic tab-content">
            <div class="tab-pane active" id="pic-1"><img src="{{asset('/storage/images/'.$immeuble->images[0])}}" /></div>
            @foreach($immeuble->images as $index => $image)
            <div class="tab-pane" id="pic-{{$index+1}}"><img src="{{asset('/storage/images/'.$image)}}" /></div>
            @endforeach
          </div>

        </div>
        <ul class="preview-thumbnail nav nav-tabs">
          @foreach($immeuble->images as $index => $image)
          <li class="{{ $index == 0 ? 'active' : '' }}"><a data-target="#pic-{{$index+1}}" data-toggle="tab"><img src="{{asset('/storage/images/'.$image)}}" /></a></li>
          @endforeach

        </ul>

      </div>
      <div class="details col-md-6">
        <h3 class="product-title">
          {{$immeuble->type}}
        </h3>
        <h4 class="price">

          Type location: <span> {{$immeuble->type_location}}</span></h4>

        <h4 class="price">

          Description: <span> {{$immeuble->description}}</span></h4>
        <h4 class="price">

          Telephone: <span> {{$immeuble->telephone}}</span></h4>
        <h4 class="price">

          Price: <span> {{$immeuble->prix}}</span></h4>

        <h4 class="price">
          Ville: <span> {{$immeuble->ville}}</span></h4>

        <h4 class="price">
          Etage: <span> {{$immeuble->etage}}</span></h4>
        <h4 class="price">
          Surface: <span> {{$immeuble->surface}}</span></h4>
        <h4 class="price">Region :<span>{{$immeuble->region}}</span></h4>
        <h4 class="price"> Nombre chambre :<span>{{$immeuble->nbr_chambre }}</span></h4>
        <h4 class="price">Autre information :
      @foreach($immeuble->info as $info)
        <span>{{$info}}.</span>
@endforeach
 </h4>
        <h4 class="price">Address :<span>{{$immeuble->addresse}}</span></h4>

      </div>
    </div>
  </div>
</div>
</div>
@endsection