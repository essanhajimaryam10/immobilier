@extends('layouts.head')
<!-- <head> -->
  <!-- bootstrap core css -->
  <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css" /> -->
  <!-- fonts style -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Poppins:400,700|Raleway:400,700&display=swap" rel="stylesheet"> -->
  <!-- Custom styles for this template -->
  <!-- <link href="css/style.css" rel="stylesheet" /> -->
  <!-- responsive style -->
  <!-- <link href="css/responsive.css" rel="stylesheet" /> -->
  <!-- Bootstrap CSS -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
  <!-- Custom CSS -->
  @section('css')
  <style>
    
    @media (max-width: 1120px) {}

@media (max-width: 992px) {
  .User_option {
    display: none;
  }

  .sale_section .sale_container .box {
    -ms-flex-preferred-size: 48%;
    flex-basis: 48%;
    margin: 1%;
  }

  .sale_section .sale_container .box {
    -ms-flex-preferred-size: 98%;
    flex-basis: 98%;
    margin: 1%;

  }
}

@media (max-width: 768px) {
  .hero_area {
    background: none;
    background-color: #e9eefc;
    height: auto;
  }


  .custom_menu-btn button span {
    background-color: #0f2480;
  }

  .sub_page .custom_menu-btn button span {
    background-color: #ffffff;
  }


  .slider_section {
    text-align: center;
  }

  .slider_section .detail-box {
    margin-bottom: 45px;
  }

  .slider_section {
    padding: 35px 0 75px 0;
  }


  


  .deal_section .img-box {
    width: 100%;
    margin-top: 45px;
  }

  .info_section .row>div {
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    margin: 15px 0;
  }

  .contact_section .row {
    flex-direction: column-reverse;
  }

  .contact_section .map_container {
    padding: 0;
    height: 450px;
  }

  .contact_section .form_container {
    padding: 45px 0;
  }

  .client_section .client_container {
    width: 95%;
  }

  .client_section .client_container .box {
    padding-bottom: 45px;
  }

  .client_section .client_container .carousel-control-prev,
  .client_section .client_container .carousel-control-next {
    top: calc(100% - 45px);
  }

  .client_section .client_container .carousel-control-prev {
    left: calc(50% - 50px);
  }

  .client_section .client_container .carousel-control-next {
    right: calc(50% - 50px);

  }

  .info_contact {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .info_section .info_contact .img-box {
    margin-top: 5px;
  }

  .info_section .info_contact>div {
    align-items: flex-start;
  }

  .info_section .info_form h5 {
    text-align: center;
  }

  .info_section .info_form .social_box {
    justify-content: center;
  }

  .info_section .info_form .social_box a {
    margin: 0 15px;
  }

  .footer_section p {
    width: 85%;
  }
}

@media (max-width: 576px) {
  .slider_section .detail-box h1 {
    font-size: 3rem;
  }

  .client_section .client_container div#carouselExampleControls {
    padding: 0 5%;
  }

  .client_section .client_container .box {
    flex-direction: column;
    align-items: center;

  }

  .client_section .client_container .box .detail-box {
    margin-top: 25px;
  }

  .client_section .client_container div#carouselExampleControls::before {
    height: 165px;
    left: 30%;
    top: 43px;

  }

  .footer_section p {
    width: 100%;
  }
}

@media (max-width: 480px) {}

@media (max-width: 420px) {
  .client_section .client_container div#carouselExampleControls::before {
    left: 20%;
  }
}

@media (max-width: 360px) {}

@media (min-width: 1200px) {
  .container {
    max-width: 1170px;
  }
}
  body {
  font-family: 'Poppins', sans-serif;
  color: #0c0c0c;
  background-color: #f8f9fa;
}

.layout_padding {
  padding-top: 90px;
  padding-bottom: 90px;
}

.layout_padding2 {
  padding: 45px 0;
}

.layout_padding2-top {
  padding-top: 45px;
  padding-bottom: 45px;
}

.layout_padding2-bottom {
  padding-bottom: 45px;
}

.layout_padding-top {
  padding-top: 90px;
}

.layout_padding-bottom {
  padding-bottom: 90px;
}

.heading_container {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}

.heading_container h2 {
  padding-left: 5px;
  position: relative;
  border-left: 7px solid #0f2480;
  font-weight: bold;
}

/*header section*/
.hero_area {
  background-image: url('/assets/images/hero-bg.jpg');
  position: relative;
  background-repeat: no-repeat;
  background-position: top right;
  /* background-size: 60% 100%; */
}

.sub_page .hero_area {
  height: auto;
  background: none;
  background-color: #0f2480;
}

.header_section {
  overflow-x: hidden;
}

.header_section .container-fluid {
  padding-right: 25px;
  padding-left: 25px;
}

.header_section .nav_container {
  margin: 0 auto;
}

.header_section .navbar-expand-lg .navbar-nav {
  width: 90%;
}

.header_section .user_option {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.header_section .user_option a {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  color: #ffffff;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.custom_nav-container.navbar-expand-lg .navbar-nav .nav-item .nav-link {
  padding: 10px 15px;
  color: #060606;
  text-align: center;
  text-transform: uppercase;
}

.custom_nav-container.navbar-expand-lg .navbar-nav .nav-item.active .nav-link, .custom_nav-container.navbar-expand-lg .navbar-nav .nav-item:hover .nav-link {
  color: #3554d1;
}

a,
a:hover,
a:focus {
  text-decoration: none;
}

a:hover,
a:focus {
  color: initial;
}

.btn,
.btn:focus {
  outline: none !important;
  -webkit-box-shadow: none;
          box-shadow: none;
}

.custom_nav-container .nav_search-btn {
  background-image: url(../images/search-icon.png);
  background-size: 17px;
  background-repeat: no-repeat;
  background-position-y: 7px;
  width: 35px;
  height: 35px;
  padding: 0;
  border: none;
}

.navbar-brand {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.navbar-brand img {
  width: 90px;
}

.custom_nav-container {
  z-index: 99999;
  padding: 0;
  height: 80px;
}

.custom_nav-container .navbar-toggler {
  outline: none;
}

.custom_nav-container .navbar-toggler .navbar-toggler-icon {
  background-image: url(../images/menu.png);
  background-size: 50px;
  width: 30px;
  height: 30px;
}

.lg_toggl-btn {
  background-color: transparent;
  border: none;
  outline: none;
  width: 56px;
  height: 40px;
  cursor: pointer;
}

.lg_toggl-btn:focus {
  outline: none;
}

.User_option {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  margin-left: auto;
  margin-right: 35px;
}

.User_option a {
  color: #ffffff;
}

.custom_menu-btn {
  z-index: 9;
  position: absolute;
  right: 15px;
  top: 7px;
}

.custom_menu-btn button {
  margin-top: 12px;
  outline: none;
  border: none;
  background-color: transparent;
}

.custom_menu-btn button span {
  display: block;
  width: 50px;
  height: 5px;
  background-color: #fff;
  margin: 6px 0;
  -webkit-transition: all .3s;
  transition: all .3s;
}

.menu_btn-style {
  position: fixed;
  right: 40px;
  top: 7px;
}

.custom_menu-btn .s-2 {
  width: 25px;
  margin-left: auto;
}

.custom_menu-btn .s-3 {
  width: 25px;
  margin-left: auto;
}

.overlay {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: black;
  background-color: rgba(53, 84, 209, 0.9);
  overflow-x: hidden;
  -webkit-transition: 0.5s;
  transition: 0.5s;
}

.overlay .closebtn {
  position: absolute;
  top: 0;
  right: 30px;
  font-size: 60px;
}

.overlay a {
  padding: 0px;
  text-decoration: none;
  font-size: 22px;
  color: #f1f1f1;
  display: block;
  -webkit-transition: 0.3s;
  transition: 0.3s;
  margin-bottom: 15px;
}

.overlay a:hover {
  color: #252525;
}

.overlay-content {
  position: relative;
  top: 30%;
  width: 100%;
  text-align: center;
  margin-top: 30px;
}

.menu_width {
  width: 100%;
}

/*end header section*/
/* slider section */
 .slider_section {
  padding: 75px 0;
  color: #0b0b0b;
}

.slider_section .container {
  position: relative;
  z-index: 2;
}

.slider_section .row {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.slider_section .detail-box h1 {
  color: #3554d1;
  font-weight: bold;
  font-size: 4rem;
}

.slider_section .detail-box h1 span {
  color: #0f2480;
}

.slider_section .detail-box p {
  margin-top: 25px;
}

.slider_section .detail-box .btn-box {
  margin-top: 45px;
}

.slider_section .detail-box .btn-box a {
  display: inline-block;
  padding: 8px 35px;
  background-color: transparent;
  color: #3554d1;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  border: 1px solid #3554d1;
  border-radius: 25px;
  margin-bottom: 75px;
}
*/
.slider_section .detail-box .btn-box a:hover {
  background-color: #3554d1;
  border-color: transparent;
  color: #ffffff;
} 


.about_section {
  position: relative;
  padding-top: 75px;
}

.about_section .row {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}

.about_section .img-box img {
  width: 100%;
}

.about_section .detail-box p {
  margin-top: 15px;
}

.about_section .detail-box a {
  display: inline-block;
  padding: 7px 30px;
  background-color: transparent;
  color: #3554d1;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  border: 1px solid #3554d1;
  border-radius: 25px;
  margin-top: 25px;
}

.about_section .detail-box a:hover {
  background-color: #3554d1;
  border-color: transparent;
  color: #ffffff;
}

.about_section .square-box {
  position: absolute;
  right: 25px;
  bottom: 0;
  width: 75px;



  
}

.about_section .square-box img {
  width: 100%;
}

.sale_section .heading_container {
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  margin-bottom: 35px;
}

.sale_section .sale_container {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}

.sale_section .sale_container .box {
  -ms-flex-preferred-size: 32%;
      flex-basis: 32%;
  margin: .65%;
}

.sale_section .sale_container .box .img-box img {
  width: 100%;
}

.sale_section .sale_container .box .detail-box {
  margin-top: 10px;
}

.sale_section .sale_container .box .detail-box h6 {
  text-transform: uppercase;
  font-weight: bold;
  font-size: 18px;
}

.sale_section .btn-box {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.sale_section .btn-box a {
  display: inline-block;
  padding: 7px 30px;
  background-color: transparent;
  color: #3554d1;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
  border: 1px solid #3554d1;
  border-radius: 25px;
  margin-top: 35px;
}

.sale_section .btn-box a:hover {
  background-color: #3554d1;
  border-color: transparent;
  color: #ffffff;
}


.info_section {
  background-color: #0f2480;
  color: #ffffff;
  padding: 20px 0 35px 0;
}

.info_section h5 {
  margin-bottom: 25px;
  font-size: 24px;
}

.info_section .info_links ul {
  padding: 0;
}

.info_section .info_links ul li {
  list-style-type: none;
}

.info_section .info_links ul li a {
  color: #ffffff;
}

.info_section .info_contact .img-box {
  width: 35px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.info_section .info_contact p {
  margin: 0;
}

.info_section .info_contact > div {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  margin: 20px 0;
}

.info_section .info_contact > div img {
  height: auto;
  margin-right: 12px;
}

.info_section .info_form form input {
  outline: none;
  width: 100%;
  padding: 7px 10px;
}

.info_section .info_form form button {
  padding: 8px 35px;
  outline: none;
  border: none;
  color: #ffffff;
  background: #f97616;
  margin-top: 15px;
  text-transform: uppercase;
}

.info_section .info_form .social_box {
  margin-top: 35px;
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.info_section .info_form .social_box a {
  margin-right: 25px;
}

/* footer section*/
.footer_section {
  background-color: #0f2480;
}

.footer_section p {
  border-top: 1px solid #ffffff;
  color: #ffffff;
  margin: 0;
  width: 70%;
  padding: 25px 0 20px 0;
  margin: 0 auto;
  text-align: center;
}

.footer_section a {
  color: #ffffff;
}
#navbarNav{
  margin-left:300px ;
}

/* end footer section*/
/*# sourceMappingURL=style.css.map */


</style>
@endsection
<!-- </head> -->
<body>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light" >
    <a class="navbar-brand" href="#" ><h1>Logo</h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
      <li class="nav-item "><a href="#accueil" class="nav-link"><h5>Accueil</h5></a></li>
      <li class="nav-item "><a href="#location" class="nav-link"><h5>Location</h5></a></li>
      <li  class="nav-item"><a href="#contact" class="nav-link"><h5>Contact</h5></a></li>
      <li  class="nav-item"><a href="#about" class="nav-link"><h5>About</h5></a></li>

      @guest
      <li class="nav-item"><a  href="{{ route('login') }}" class="nav-link"><h5>Connexion</h5></a></li>
      <li class="nav-item"><a  href="{{ route('register-user') }}" class="nav-link"><h5>Register</h5></a></li>
      @else
      <li class="nav-item"><a  href="{{ route('signout') }}" class="nav-link"><h5>Se deconnecter</h5></a></li>
      @endguest
</ul>
      </div>
  </nav>

  <!-- Main content -->
  <!-- slider section -->
  
  <section class="slider_section "  id="accueil">
  <!-- <div  class='hero_area' >
  <img  src="{{asset('assets/images/hero-bg.jpg.jpg')}}" >
  </div> -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4 offset-md-1">
            <div class="detail-box">
            <div class="square-box">
      <img src="{{asset('assets/images/square.jpg')}}" alt="">
    </div>
              <h1>
                <span> Moderne</span> <br>
                Appartement <br>
                Loger
              </h1>
              <div class="btn-box">
                <a href="" class="">
                  En savoir plus
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>

  <!-- about section -->

<section class="about_section layout_padding-bottom" id="about">
    <div class="square-box">
      <img src="{{asset('assets/images/square.png')}}" alt="">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="img-box">
            <img src="{{asset('assets/images/about-img.jpg')}}" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="detail-box">
            <div class="heading_container">
              <h2>
              A propos de notre appartement              </h2>
            </div>
            <p>
            Notre service de location d'appartement offre une expérience de qualité pour les locataires. Nous travaillons dur pour offrir des appartements propres et bien entretenus. Nous sommes également fiers d'offrir un excellent service à la clientèle, répondant rapidement aux demandes et aux préoccupations des locataires.
            </p>
            <a href="">
            En savoir plus            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->
    <!-- sale section -->

    <section class="sale_section layout_padding-bottom" id="location">
    <div class="container-fluid">
      <div class="heading_container">
        <h2>
        Maison à loyer        </h2>
        <p>
        Trouvez la maison de vos rêves avec notre service de location. </p>
      </div>
      <div class="container">
      <div class="sale_container">
 <div class="row">
 @foreach($immeubles as $immeuble)

  <div class="col-lg-4 d-flex align-items-stretch justify-content-center" >
    <div class="card">
    <img class="card-img-top img-fluid" style="height:200px;object-fit:cover;width:300px" src="{{asset('/storage/images/'.$immeuble->images[0])}}" alt="the image alt text here">
      <div class="card-body ">
        <h5 class="card-title"> {{$immeuble->type}}</h5>
        <p class="card-text "> {{$immeuble->description}}</p>
        <a href="{{ route('home.show', ['id'=> $immeuble->id]) }}" class="btn btn-primary">Voir info</a>
      </div>
    </div>
  </div>
  @endforeach

  </div> 

</div>

 
     
      </div>
    </div>
  </section>

<!-- end sale section -->
  <!-- info section -->
  <footer>

  <section class="info_section " id="contact">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="info_contact">
            <h5>
            A propos de nous            </h5>
            <div>
              <div class="img-box">
              
                <img src="{{asset('assets/images/location.png')}}" width="18px" alt="">
              </div>
              <p>
                Address
              </p>
            </div>
            <div>
              <div class="img-box">
                <img src="{{asset('assets/images/phone.png')}}" width="12px" alt="">
              </div>
              <p>
                +212 657809007
              </p>
            </div>
            <div>
              <div class="img-box">
                <img src="{{asset('assets/images/mail.png')}}" width="18px" alt="">
              </div>
              <p>
                mariem@gmail.com
              </p>
            </div>
          </div>
        </div>
        
        <div class="col-md-3">
          <div class="info_form ">
            <h5>
            Contact            </h5>
            <div class="social_box">
              <a href="">
              <!-- {{asset('assets/images/fb.png')}} -->
                <img src="{{asset('assets/images/fb.png')}}" alt="">
              </a>
              <a href="">
                <img src="{{asset('assets/images/twitter.png')}}" alt="">
              </a>
              <a href="">
              <img src="{{asset('assets/images/linkedin.png')}}" alt="">
              </a>
              <a href="">
              <img src="{{asset('assets/images/youtube.png')}}" alt="">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  </footer>

  <!-- end info_section -->


  <!-- footer section -->
  <section class="container-fluid footer_section ">
    <div class="container">
      <p>
        &copy; <span id="displayYear"></span> Tous droits réservés par
        <a href="">Immobilier</a>
      </p>
    </div>
  </section>
  <!-- end  footer section -->





  @section('scripts')





  <!-- Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"> </script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  @endsection
</body>
</html>
