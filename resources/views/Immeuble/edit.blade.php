@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
 Immeuble
@endsection



@section('content')

<div class="card mb-4">
 @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif 
  <div class="card-header">
 Edit Immeuble
  </div>
  
  <div class="card-body">
  <form method="POST" action="{{ route('immeubles.update',$immeuble->id) }}"enctype="multipart/form-data">
      @csrf
   @method('PUT')
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label for="type" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type d'immeuble</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="type" value='{{$immeuble->type}}'name="type">
                    <option value="appartement"{{ $immeuble->type == 'appartement' ? 'selected' : '' }}>Appartement</option>
                    <option value="atelier" {{ $immeuble->type == 'atelier' ? 'selected' : '' }} >Atelier</option>
                    <option value="boutique" {{ $immeuble->type == 'boutique' ? 'selected' : '' }}>Boutique</option>
                    <option value="bureaux" {{ $immeuble->type == 'bureaux' ? 'selected' : '' }}>Bureaux</option>
                    <option value="chambre" {{ $immeuble->type == 'chambre' ? 'selected' : '' }}>Chambre</option>
                    <option value="garage" {{ $immeuble->type == 'garage' ? 'selected' : '' }}>Garage</option>
                    <option value="local" {{ $immeuble->type == 'loca' ? 'selected' : '' }}>local professionnel</option>
                    <option value="studio" {{ $immeuble->type == 'studio' ? 'selected' : '' }}>Studio</option>
                    <option value="maison" {{ $immeuble->type == 'maison' ? 'selected' : '' }}>Maison</option>
                    <option value="autre" {{ $immeuble->type == 'autr' ? 'selected' : '' }}>Autre</option>
                </select>
     </div>
   </div>
   <div class="col">
          <div class="mb-3 row">
          <label for="type_location" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type de location</label> <br>
              <select class="col-lg-10 col-md-6 col-sm-12" id="type_location" value="{{ $immeuble->type_location}}"  name="type_location">
                <option value="meublée" {{ $immeuble->type_location == 'meublée' ? 'selected' : '' }}>Meublée</option>
                <option  value="vide"  {{ $immeuble->type_location == 'vide' ? 'selected' : '' }}>Vide</option>
              </select>
  </div>
    </div>
    </div>

<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adresse</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="addresse" value="{{ $immeuble->addresse}}" type="text" class="form-control">
            </div>
          </div>
        </div>
       
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Telephone</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="telephone"  value="{{ $immeuble->telephone}}"  placeholder="Telephone " type="text" class="form-control">

            </div>
          </div>
        </div>

        </div> 
        <div class="row">

        <div class="col">
          
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Image</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
            </div>
          </div>
        </div>
        </div>


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Ville</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="ville" value="{{  $immeuble->ville }}" type="text" class="form-control">
            </div>
          </div>
        </div>
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Code postal</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="code" value="{{ $immeuble->code }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Region</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="region" value="{{ $immeuble->region }}" type="text" class="form-control">
            </div>
          </div>
        </div>
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Surface m2</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="surface" value="{{  $immeuble->surface }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Etage</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="etage" value="{{  $immeuble->etage }}" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Prix</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="prix" value="{{ $immeuble->prix}}" type="number" class="form-control">
              </div>
            </div>
          </div> 

        </div> 


<div class="row">
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Description</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="description" value="{{  $immeuble->description }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Nombre de chambres</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="nbr_chambre" value="{{ $immeuble->nbr_chambre }}" type="number" class="form-control">
            </div>
          </div>
        </div>

        </div>

        <div class="row">
        <div class="col">
          
        <div class="mb-3 row">
            <label class="checkbox-inline col-lg-2 col-md-6 col-sm-12 col-form-label" >Informations complémentaires</label>
            <input type="checkbox"  value="Accès Internet" name="info[]">Accès Internet <br>
            <input type="checkbox" value="Balcon" name="info[]"> Balcon <br>
            <input type="checkbox" value="Chauffage collectif" name="info[]">Chauffage collectif <br>
            <input type="checkbox" value="Interphone"name="info[]">Interphone <br>
            <input type="checkbox" value="Système de sécurité" name="info[]"> Système de sécurité <br>

          </div>
    </div>
    </div>
       

                          
    
  
                 <a class="btn btn-primary" href="{{ route('immeubles.index') }}"> Retour</a>
        <button type="submit" class="btn btn-primary">Ajouter</button>
        
</form>

</div>


</div>



</div>
</div>












@endsection