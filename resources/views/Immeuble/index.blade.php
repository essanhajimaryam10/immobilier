@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
Locataires
@endsection


@section('content')
    <div class="row">
    @if (session()->has('error'))
        <div class="alert alert-danger  bg-danger " id="success-alert">
            <div class="d-flex align-items-center">
               
                <div class="ms-3">
                    <!-- <h6 class="mb-0 text-white">Error</h6> -->
                    <div class="text-white">{{ session()->get('error') }}</div>
                </div>
            </div>
          
        </div>
    @endif 
    </div>
    <div class="dropdown-divider border-black"></div>
    <div class="row ml-4 ">
        <div class="col-md-6">
            <h5 class="fw-bold mb-0">liste des Immeubles</h5>
        </div>
        
    </div>
    <div class="dropdown-divider border-black"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                    <div class="button">
                        <a href="{{ route('immeubles.create') }}" class="btn btn-sm btn-primary">nouveau Immeuble</a>
                        </div>
                        <br>
         <form method="GET">
        <div class="input-group mb-3">
          <input 
            type="text" 
            name="search" 
            value="" 
            class="form-control" 
            placeholder="recherche par ville  et type d'immeuble ..." 
            aria-label="Search" 
            aria-describedby="button-addon2">
          <button class="btn btn-success" type="submit" id="button-addon2">Recherche</button>
        </div>
    </form>
                       

                        <table class="table table-responsive-sm table-bordered table-striped">
                            <thead>
                                <th>Type d'immeuble</th>
                                <th>Type de location</th>
                                <th>Ville</th>

                                <th>Superficie m2</th>
                               
                                <th>prix</th>
                                <th>Action</th>
                            </thead>
                            <tbody>

                                @foreach ($immeubles as $immeuble)
                                <tr>
                        <td>{{ $immeuble->type}}</td>
                         <td>{{ $immeuble->type_location}}</td>
                        <td>{{ $immeuble->ville}}</td>
                        <!-- <td >
                                        @foreach($immeuble->images as $image)
                                        <img src="{{asset('/storage/images/'.$image)}}" alt="multiple image"
                                            class="w-20 h-20 border border-blue-600">
                                        @endforeach
                                    </td> -->
                        <td>{{ $immeuble->surface}}</td>
                      
                        <td>{{ $immeuble->prix}}</td>
                        <td class="text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn btn-success" href="{{ route('immeubles.edit',$immeuble->id) }}">modifier</a>
                                        </div>
                                        <div class="col-md-6">

                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$immeuble->id }}">Supprimer</button>
                                        </div>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal{{ $immeuble->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <center>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h3 class="modal-title">Supprimer la confirmation d'immeuble</h3><br>
                                                </div>
                                            </center>
                                                <div class="modal-body">
                                                    <p>Êtes-vous sûr de vouloir supprimer cet immeuble ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-hover btn-primary" data-dismiss="modal">Annuler</button>
                                                    <form action="{{ route('immeubles.destroy',$immeuble->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                      
                                       
                                    </div>

                                </td>
                        
                        
                    </tr>
                    @endforeach    
                  
                                

                           
                            </tbody>
                        </table>
                        @if (isset($message))
    <div class="alert alert-info">{{ $message }}</div>
@endif
                    </div>
                    <div class="card-body d-flex justify-content-left">
            {!! $immeubles->links() !!}
        </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

@endsection 