@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
 Immeuble
@endsection



@section('content')

<div class="card mb-4">
 @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif 
  <div class="card-header">
  Creer nouveau Immeuble
  </div>
  <div class="card-body">
  <form method="POST" action="{{ route('immeubles.store') }}"enctype="multipart/form-data">
      @csrf

      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label for="type" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type d'immeuble</label> <br>
                  <select class="col-lg-10 col-md-6 col-sm-12" id="type"  name="type">
                  <option value="">Selecte type d'immeuble</option>
                    <option value="appartement">Appartement</option>
                    <option value="atelier">Atelier</option>
                    <option value="boutique">Boutique</option>
                    <option value="bureaux">Bureaux</option>
                    <option value="chambre">Chambre</option>
                    <option value="garage">Garage</option>
                    <option value="local">local professionnel</option>
                    <option value="studio">Studio</option>
                    <option value="maison">Maison</option>
                    <option value="autre">Autre</option>
                </select>
     </div>
   </div>
   <div class="col">
          <div class="mb-3 row">
          <label for="type_location" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type de location</label> <br>
              <select class="col-lg-10 col-md-6 col-sm-12" id="type_location"  name="type_location">
              <option value="">Selecte type Location</option>
               
              <option value="meublée">Meublée</option>
                <option  value="vide">Vide</option>
              </select>
  </div>
    </div>
    </div>

<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adresse</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="addresse" value="{{ old('addresse') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Telephone</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
            <input name="telephone"  value=""  placeholder="Telephone " type="text" class="form-control">

            </div>
          </div>
        </div>


        </div> 
<div class="row">

        <div class="col">
          
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Image</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
              <input class="form-control" type="file" name="images[]" >
            </div>
          </div>
        </div>
        </div>

<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Ville</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="ville" value="{{ old('ville') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Code postal</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="code" value="{{ old('code') }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Region</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="region" value="{{ old('region') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Surface m2</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="surface" value="{{ old('surface') }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        </div> 


<div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Etage</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="etage" value="{{ old('etage') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Prix</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="prix" value="{{ old('prix') }}" type="number" class="form-control">
              </div>
            </div>
          </div> 

        </div> 


<div class="row">
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Description</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="description"  type="text" class="form-control">
              </div>
            </div>
          </div> 

        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Nombre de chambres</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="nbr_chambre"
               type="number" class="form-control">
            </div>
          </div>
        </div>

        </div>

        <div class="row">
        <div class="col">
          
        <div class="mb-3 row">
            <label class="checkbox-inline col-lg-2 col-md-6 col-sm-12 col-form-label" >Informations complémentaires</label>
            <input type="checkbox" value="Accès Internet" name="info[]">Accès Internet <br>
            <input type="checkbox" value="Balcon" name="info[]"> Balcon <br>
            <input type="checkbox" value="Chauffage collectif" name="info[]">Chauffage collectif <br>
            <input type="checkbox" value="Interphone"name="info[]">Interphone <br>
            <input type="checkbox" value="Système de sécurité" name="info[]"> Système de sécurité <br>

          </div>
    </div>
    </div>
       

       
    <a class="btn btn-primary" href="{{ route('immeubles.index') }}"> Retour</a>

        <button type="submit" class="btn btn-primary">Ajouter</button>
        
</form>

</div>


</div>



</div>
</div>












@endsection