@extends('layouts.master')

@section('title')
Home
@stop

@section('css')
<style>
   
    .titre {
    padding:100px;
    color: #0f2480;
   



    }
    .button{
    border-radius: 1rem;
    border: none;
    cursor: pointer;
    font-weight: 600;
    color: #fff;
    background-color: #3554d1;
    padding: 16px 34px;
   
    }
    @media only screen and (max-width: 768px) {
  /* For mobile phones: */
  .button {
   
    /* margin: 102px;   */
    margin: 2%;

  }
}  

</style>
@endsection

@section('title_page1')
Home
@endsection


@section('content')
<!-- <section class="content"> -->
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-4 col-12">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$totalLocataires}}</h3>

                            <p>Nouveau Locataires</p>
                        </div>
                        <!-- <div class="icon">
                             <i class="ion ion-person-add"></i>
                        </div> -->
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-12">
                     <!-- small box -->
                     <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$totalImmeubles}}<sup style="font-size: 20px"></sup></h3>

                            <p>Nombre Immeubles</p>
                        </div>
                        <!-- <div class="icon">
                        <iconify-icon icon="material-symbols:add-home"></iconify-icon>
                        </div> -->
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-4 col-12">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{ $totalLocation }}</h3>

                            <p>Nombre Location</p>
                        </div>
                        <!-- <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div> -->
                        {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
                    </div>
                </div>
                <!-- ./col -->
                <!-- <div class="col-lg-3 col-6">
                    
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div> -->
                <!-- ./col -->
            </div>
                                
<div class="titre">
    <h1><b>Bienvenue </b> </h1>
    <h3 style='margin-bottom: 22PX;'>Merci de vous être inscrit ! Nous sommes heureux de vous avoir à bord !
    <br> Dites nous un peu plus sur vous afin de completer votre profil.</h3>
<a class="button" href="/landlord">Compléte</a>

</div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</section>
<!-- right col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
<!-- </section> -->

                 

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</section>
<!-- right col -->
</div>
<!-- /.row (main row) -->
</div><!-- /.container-fluid -->
<!-- </section> -->

                 


@endsection


@section('scripts')

@endsection