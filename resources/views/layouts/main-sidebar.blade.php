<style>
  .elevation-4{
    background-color: #0f2480;
;
  }
  .elevation-4 ul li:hover,
.elevation-4 ul li.hovered {
  background-color: #3554d1;
}
</style>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">

        <span class="brand-text font-weight-light">Landlord</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">

            </div>
            <div class="info">
                <a href="#" class="d-block">immobilier
</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
       

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/dashboard" class="nav-link ">
                   
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAQtJREFUSEtjZKAxYKSx+QxEW9A//73A718s+0EOYmX741iYKPiBGMcRZQGS4QZQQy8QawlBC5ANZ2RguAiy4D8Dgz4DAwNRluC1AN1wFrY/DiAL/vxiOUCsJTgtwGY4LNxBcsRagtUCfIbDIpZYSzAsIMZwUixBsYAUw4m1BG4BelJkZfsjSGxah+p9jy0Jwy3onvn5AjRlgNWVpfMSTMLIGa1r5uf/SPwLZem8hiA+rSw4WJbOC07SGK6EuYRcH6DrG7UAHq+4gnY0iMgJok8PGBgY5YmprdDVgOqL0nReWKUElsaIg86ZnwKYGBgbkHM1cZb9f/ifgaGgPJ1vA7J6kooD4ixCVUVzCwBtuuIZdm2QzAAAAABJRU5ErkJggg=="/>
                         <p>  Home 
                        </p>
                   
                    </a>
                </li>
               
                <li class="nav-item">
                    <a href="/landlord " class="nav-link ">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAatJREFUSEu1lTFSwkAUhv+HWbQwCjfwCHgC8AZ6AqHK2EFBKIHSUGDnxIbxBtxAOYEcgRsQjIVjgHU2CZrAJpsBkjab9/3vvX//EHJ+KOf6yAQYPLs1foIuOCq+IMKUVui3H/R3lUAl4NF2ewR0ZYU40O8Yei8NkgrwlRfwBmAB4k3GVuNWo+xYL591cHoCcElr3KR1kgqwbFeMoMrB7zrGxTiqNISMAExMQ68ldaECOEKlaeg754ajecn70eYAHNPQy3kCFqahl/YF5DuiyJId4ryuna4mkSUPAZQOWrJoO1ebbuYadiL8Hlw0YEpr9I5y0VQ3VfU+0aa+DT12D/DbMCK2neKIyABozJj3KnYjg0kBwUhoBPArlcLgPc1ozRuyke0AIs4RX07ErLWz5XRboehw+a1VeAFiN1UfI4mNGCC4nexDKM8SZJvu/p1GM1b0rqNiYoCB/dXk4MLfqfkiG9smtwjUahvnIgjDZI+c/jukSEgZIDLamLhYB5bt+uHGistykiuSlp4UftsALgrI0jOLmyzb3fle+UfLUnjvP9qhxX3rHqNIWo1fPZjXGUWd5ycAAAAASUVORK5CYII="/>
                        <p>
                        Landlord 
                        </p>
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="/locataires" class="nav-link ">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAgBJREFUSEvVVU1y0gAU/h4huElQTqA9gfQEtiewnkDYkHYlLgjjSlw5DYvCStNN0xPIDcQTGE8gnqAM6QpCnvOSJg1pEsqMnalZ5e+9733f+76E8MAHPXB/PF6AU9sbBIHvfDhpzMpUKGUwtK+7TPwajAMQpgBNzI42PrUXRwT6BmCu1vy99+3GvAikEMD66o1AeHenkDFWn/gDf1mdMvCSwJc9o97aCcA6v26C+WdYRNw2O3XHOl+0wHQR3aN9VV3NVsuqyPN0Hfh7RVLlMgilAZ9lpxvaC4dBbxNQ25sCeEUBDnsnupzfOUoBwBibx3o3rvpnAGmJGNTqG9plViKzo7mWvZgB9Fwkk+t7M5AXy5YsrMSmBHwk4FfP0Js7LflWknAXR6IzgB8EmvQMbbTBBpiDaCD23YlBWXg+f7l6oSjKATF1xarhu0yOeay1s3W5S76ZUDKQR91l0Ej2Is1uQueIXRn41Df0QRpkA+Ds4urZaln9XtA4O5yr1vxDSXEq2chmYgNgaHtuRJn/MNANgrWbDpBIU6koTQJGoXsA1zT0fUGOLZxlkQDE4ZLmam3dLPu+REwVN7JolPQUiwQ0DP2tY6LpGfymb9Qn2/4TeQ0t22OpMw096ZucxA/Vmt8omz4GDp1Uqf7ONix0UR76Nhb3qXm8f7Rt7OLn/z+DvzsODChDswYuAAAAAElFTkSuQmCC"/>
                        <p>
                        locataires
                        </p>
                    </a>
                </li>
               
                <li class="nav-item">
                    <a href="/immeubles" class="nav-link ">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAKJJREFUSEtjZKAxYKSx+QwYFnRP/+zwn4lxPgPDfwXSLGd8wPjvf2JpJu8BZH0YFnTN/PSAgYFRnjTDYaoZH5Sl8ygSsODzf5CCsnRekoKvayZ2fVh8QGcLYC5D9hU+MXSfE/QBzS0gNrJH4wAcUrAIpGokj8bBaD6Ap4HBm9FolkyJNRhdHTGlKajKsyfTgoNl6bwOeGs0Mg3GqY2kapEcywExUsgZhoNM0wAAAABJRU5ErkJggg=="/>
                        <p>
                            Immeuble
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/locations" class="nav-link ">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAASJJREFUSEvtlTEOgkAQRWcNUrmJnEA9gXoC9ShUG0sLOQQUlobKo6g30BtwA4x0SFwzBswKK7PEaCykIzP8N/NnmWXw4Yd9WB9qAcE6mcoW2wDIvlkhLGJX6S7nfFfk1wL88BwBsJ6Z+EMy8kRnYAhIJCZ6ghtZ6YfVfKKDLwPKFVLv2H2jDijB37JotYm7l9SKsc22nTkL1zlRp8m4g1x8CwCjXPTQtrNZGUJZpp2BKs4AjpgkAYYAUIE0BpTFLTubIiBLrd0riGpbrUU68cISjJlAagFBmBzyKrVDVYeOdnmCjxtZpAKK1fBKAGezFHzUCIBeUh/oLDCegQ5QPvd/wGMG1Eqg4ur98bRN/TDBq25CCRDxvSf4/Qcl1/WboO8AborGCij+6zXkAAAAAElFTkSuQmCC"/>
                        <p>
                            location
                        </p>
                    </a>
                </li>
               
                
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
