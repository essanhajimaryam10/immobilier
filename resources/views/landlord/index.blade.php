@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
Landlord
@endsection



@section('content')
<div class="card mb-4">
  <div class="card-header">
  Informations personnelles
  </div>
  <div class="card-body">
  @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif
   
    <form method="POST" action="{{ route('landlord.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">  Nom</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="nom" value="{{ $user->name }}"  placeholder="Nom *" type="text" class="form-control">
            </div>
          </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Telephone</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="telephone"  value="{{ $user->telephone }}"  placeholder="Telephone *" type="text" class="form-control">
              </div>
            </div>
          </div> 
        </div>
        <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Email </label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="email" value="{{ $user->email }}" placeholder="Email *" type="email" class="form-control">
            </div>
          </div>
        </div>
    
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Adresse	</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" placeholder="Adresse *" type="text" name="address">
              
            </div>
           
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Ville</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" placeholder="Ville *" name="ville">
              </div>
           </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Code Postal	</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" placeholder="Code Postal *" name="codePostal">
              </div>
            </div>
          </div>
        
      </div> 
      <div class="row">
        <div class="col">
          <div class="mb-3 row">
            <label for="civilite" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Civilite
</label> <br>
      <select class="col-lg-10 col-md-6 col-sm-12" id="civilite"  name="civilite">
      <option  value="">Select civilite</option>
        <option value="mll
        ">Mll
        </option>
        <option  value="mlle">Mlle</option>
        <option value="mme">Mme</option>
    </select>
  </div>
</div>
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Region	</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" placeholder='Region ' name="region">
              </div>
            </div>
          </div>
</div>

<div class="row">
       
<div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Societe	</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" placeholder=' Nom Societe' name="societe">
              </div>
            </div>
          </div>
         <div class="col">
          &nbsp;
        </div> 
</div>

     
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>


     
    
  </div>
</div>
@endsection
