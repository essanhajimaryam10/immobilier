@extends('layouts.head')

@section('css')
<style>
    .register{
    background: -webkit-linear-gradient(left, #0f2480, #00c6ff);
    margin-top: 3%;
    padding: 3%;
}
.register-left{
    text-align: center;
    color: #fff;
    margin-top: 4%;
}
.register-left input{
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    width: 60%;
    background: #f8f9fa;
    font-weight: bold;
    color: #383d41;
    margin-top: 30%;
    margin-bottom: 3%;
    cursor: pointer;
}
a{
   
    border: none;
    border-radius: 1.5rem;
    padding: 10px 15px;
    text-decoration:none;
    width: 60%;
    background: #f8f9fa;
    font-weight: bold;
    color: #383d41;
    margin-top: 30%;
    margin-bottom: 3%;
    cursor: pointer;

}
.register-right{
    background: #f8f9fa;
    border-top-left-radius: 10% 50%;
    border-bottom-left-radius: 10% 50%;
}
.register-left img{
    margin-top: 15%;
    margin-bottom: 5%;
    width: 25%;
    -webkit-animation: mover 2s infinite  alternate;
    animation: mover 1s infinite  alternate;
}
@-webkit-keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
@keyframes mover {
    0% { transform: translateY(0); }
    100% { transform: translateY(-20px); }
}
.register-left p{
    font-weight: lighter;
    padding: 12%;
    margin-top: -9%;
}
.register .register-form{
    padding: 10%;
    margin-top: 10%;
}
.btnRegister{
    float: right;
    margin-top: 10%;
    border: none;
    border-radius: 1.5rem;
    padding: 2%;
    background: #0062cc;
    color: #fff;
    font-weight: 600;
    width: 50%;
    cursor: pointer;
}
.register .nav-tabs{
    margin-top: 3%;
    border: none;
    background: #0062cc;
    border-radius: 1.5rem;
    width: 28%;
    float: right;
}
.register .nav-tabs .nav-link{
    padding: 2%;
    height: 34px;
    font-weight: 600;
    color: #fff;
    border-top-right-radius: 1.5rem;
    border-bottom-right-radius: 1.5rem;
}
.register .nav-tabs .nav-link:hover{
    border: none;
}
.register .nav-tabs .nav-link.active{
    width: 100px;
    color: #0062cc;
    border: 2px solid #0062cc;
    border-top-left-radius: 1.5rem;
    border-bottom-left-radius: 1.5rem;
}
.register-heading{
    text-align: center;
    margin-top: 8%;
    margin-bottom: -15%;
    color: #495057;
}
</style>
@endsection
<div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <h3>Bienvenue</h3>
                        <p>nous vous proposons un service personnalisé et adapté à vos attentes</p>
                        <a href='/login'>Connexion</a><br/>
                    </div>
                    <div class="col-md-9 register-right">
                       
                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Landlord</h3>
                                <form action="{{ route('register.custom') }}" method="POST">
                            @csrf
                                <div class="row register-form">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Nom *" name='name' value="" required autofocus/>
                                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder=" Email *" name='email' value="" required autofocus/>
                                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="mot de passe *" value="" name='password' required/>
                                            @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                                        </div>
                                       
                                      
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                            <input type="password" class="form-control"  placeholder="Confirmez le mot de passe  *" name='conf_password' value="" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" minlength="10" maxlength="10" name="txtEmpPhone" class="form-control" placeholder=" Telephone *" name='telephone' value="" />
                                            @if ($errors->has('Telephone'))
                                <span class="text-danger">{{ $errors->first('Telephone') }}</span>
                                @endif
                                        </div>
                                        
                                      
                                        <input type="submit" class="btnRegister"  value="S'inscrire"/>
                                    </div>
                                </div>
                            </div>
                           
                            </form>
                        </div>
                    </div>
                </div>

            </div>

@section('scripts')

@endsection