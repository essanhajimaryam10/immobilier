@extends('layoutsHome.app')

@extends('layouts.head')


@section('css')

<style>
    .login-container{
    margin-top: 5%;
    margin-bottom: 5%;
}


.login-form{
    padding: 5%;
    background: #0f2480;
    box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 9px 26px 0 rgba(0, 0, 0, 0.19);
    margin-left:240px;
}
.login-form h3{
    text-align: center;
    color: #fff;
}
.login-container form{
    padding: 10%;
}
.btnSubmit
{
    width: 50%;
    border-radius: 1rem;
    padding: 1.5%;
    border: none;
    cursor: pointer;
}

.login-form .btnSubmit{
    font-weight: 600;
    color: #0062cc;
    background-color: #fff;
}
.login-form .ForgetPwd{
    color: #fff;
    font-weight: 600;
    text-decoration: none;
}
@media only screen and (max-width: 768px) {
  /* For mobile phones: */
  .login-form {
   
    margin-left: 0;
}
  }



</style>
@endsection
@section('content1')
<div class="container login-container">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="row" style="margin-left: 38px;">
                
                <div class="col-md-6 login-form">
                    <h3>Connexion </h3>
                  
                                
                             
                    <form method="POST" action="{{ route('login.custom') }}">
                            @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Votre e-mail *" name='email' value="" required autofocus/>
                           
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Votre mot de passe *" name='password' value="" required />
                           
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Connexion" />
                           
                        </div>
                      
                        <div class="form-group">

                            <a href="registration" class="ForgetPwd" value="Register">Register</a>
                        </div>
                        <div class="form-group">

                            <a href="#" class="ForgetPwd" value="Login">Mot de passe oublié?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
@section('scripts')

@endsection
@endsection