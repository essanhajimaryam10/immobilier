@extends('layouts.master')

@section('title')
Home
@stop

@section('css')

@endsection

@section('title_page1')
 locataire
@endsection



@section('content')
<div class="card mb-4">
  <div class="card-header">
  Creer  nouveau locataire
  </div>
  <div class="card-body">
   @if($errors->any())
    <ul class="alert alert-danger list-unstyled">
      @foreach($errors->all() as $error)
      <li>- {{ $error }}</li>
      @endforeach
    </ul>
    @endif 

    <form method="POST" action="{{ route('locataires.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
      <div class="col">
          <div class="mb-3 row">
            <label for="state" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Type de locataire</label> <br>
            <select class="col-lg-10 col-md-6 col-sm-12"id="type"  name="type_locataire">
            <option value="">Selecte type locataire</option>
       
            <option value="locataire">locataire
        </option>

        <option value="particulier">Particulier</option>
        <option value="societe">Société/Autre</option>
    </select>
  </div>
</div>


        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Nom locataire</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="locataire_name" value="{{ old('locataire_name') }}" type="text" class="form-control">
            </div>
          </div>
        </div>
        
        
        </div>
        <div class="row">
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Prenom locataire</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input name="locataire_prenom" value="{{ old('locataire_prenom') }}" type="text" class="form-control">
              </div>
            </div>
          </div> 
        
        <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">E-mail</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input name="email" value="{{ old('email') }}" type="email" class="form-control">
            </div>
          </div>
        </div>
    
       
    </div>

    <div class="row">
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Ville</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" name="ville">
              </div>
           </div>
        </div>
        <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Code postal</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" name="code">
              </div>
           </div>
        </div>

      
         {{-- <div class="col">
          &nbsp;
        </div> --}}
      </div> 
      <div class="row">
    

      <div class="col">
   <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Region</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" name="region">
              </div>
            </div>
         </div>
<div class="col">
          <div class="mb-3 row">
            <label for="civilite" class="col-lg-2 col-md-6 col-sm-12 col-form-label">Civilite
</label> 
<br>
      <select class="col-lg-10 col-md-6 col-sm-12" id="civilite"  name="civilite">
      <option value="">Selecte type civilite</option>
       
      <option value="mll">M</option>
        <option value="mme">Mme</option>
    </select>
  </div>
  </div>
  </div>


  <div class="row">
  <div class="col">
            <div class="mb-3 row">
              <label class="col-lg-2 col-md-6 col-sm-12 col-form-label"> Adresse</label>
              <div class="col-lg-10 col-md-6 col-sm-12">
                <input class="form-control" type="text" name="adresse">
              </div>
            </div>
          </div>
         
         <div class="col">
          <div class="mb-3 row">
            <label class="col-lg-2 col-md-6 col-sm-12 col-form-label">Date de naissance</label>
            <div class="col-lg-10 col-md-6 col-sm-12">
              <input class="form-control" type="date" name="date">
            </div>
          </div>
        </div>
       
</div>

    <a class="btn btn-primary" href="{{ route('locataires.index') }}"> Retour</a>
     
      <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
  </div>
</div>


      
      <tbody class="bg-success">
       
        <div >
        </tr>
        </div>
        

      </tbody>
    </table>
    
  </div>
</div>
@endsection
