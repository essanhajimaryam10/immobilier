<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Locataire extends Model
{
    use HasFactory,Searchable;
    protected $fillable = ['type_locataire', 'locataire_name', 'locataire_prenom', 'email', 'ville', 'code','region', 'civilite', 'adresse', 'date'];
   
    public function toSearchableArray()
    {
        return [
            
            'ville' => $this->ville,
       

            
        ];
    }
}

