<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Immeuble extends Model
{
use  HasFactory,  Searchable;
    protected $fillable = ['type','type_location','addresse','telephone','images','ville','code','region','surface','etage','prix','description','nbr_chambre','info'];

    public function setInfoAttribute($value)
    {
        $this->attributes['info'] = json_encode($value);
    }

    public function getInfoAttribute($value)
    {
        return $this->attributes['info'] = json_decode($value);
    }
    
    public function toSearchableArray()
    {
        return [
            
            'ville' => $this->ville,
            'type' => $this->type

            
        ];
    }
    
    protected $casts = [
        'images' => 'array'
    ];
}

