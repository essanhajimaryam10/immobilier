<?php

namespace App\Models;

use App\Models\Immeuble;
use App\Models\Locataire;
// use App\Http\Controllers\ImmeubleController;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Location extends Model
{
    use  HasFactory,  Searchable;

 
        protected $fillable = ['immeuble_id','locataire_id','type_location', 'debut_bail','fin_bail','moyen_payement','payement'];
   
   public function immeuble(){
    return $this->belongsTo(Immeuble::class);
   }
   public function locataire(){
    return $this->belongsTo(Locataire::class);
   }
   public function toSearchableArray()
   {
       return [
           
           'payement' => $this->payement,

           
       ];
   }
}
