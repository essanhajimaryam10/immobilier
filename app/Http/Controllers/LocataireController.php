<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Locataire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocataireController extends Controller
{
    
    public function index(Request $request)
{
    if ($request->filled('search')) {
        $locataires = Locataire::search($request->search)->simplePaginate(3);

        if ($locataires->isEmpty()) {
            $message = 'Aucun résultat trouvé pour cette ville.';
            return view('locataires.index', compact('locataires', 'message'));
        }
    } else {
        $locataires = Locataire::simplePaginate(3);
    }

    return view('locataires.index', compact('locataires'));
}


    public function create()
    {
        return view('locataires.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'type_locataire' => 'required',
            'locataire_name' => 'required',
            'locataire_prenom' => 'required',
            'email' => 'required',
            'ville' => 'required',
            'code'=>'required',
            'region' => 'required',
            'civilite' => 'required',
            'adresse' => 'required',
            'date' => 'required',
        ]);
        Locataire::create($request->all());
        return back();
    }
    public function edit(Locataire $locataire)
    {
        return view('locataires.edit',compact('locataire'));
    }


    public function update(Request $request, Locataire $locataire)
    {
        $request->validate([
            'type_locataire' => 'required',
            'locataire_name' => 'required',
            'locataire_prenom' => 'required',
            'email' => 'required',
            'ville' => 'required',
            'code'=>'required',
            'region' => 'required',
            'civilite' => 'required',
            'adresse' => 'required',
            'date' => 'required',
        ]);
        
        $locataire->fill($request->post())->save();

        return back();
    }
    public function destroy(Request $request,  Locataire $locataire)
    {
        $location = Location::where("locataire_id", $locataire->id)->pluck('locataire_id');
        if ($location->isEmpty()) {
            if ($locataire) {
                $locataire->delete();
                session()->flash('error',"locataire supprimée avec succès !");
              
            }
        } else {
            session()->flash('error', "Impossible de supprimer   parce que locataire est déjà utilisé");
        }
        return back();
    }





}
