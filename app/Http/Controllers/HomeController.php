<?php

namespace App\Http\Controllers;

use App\Models\Immeuble;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){

        if($request->filled('search')){
            $immeubles = Immeuble::search($request->search)->get();
        }else{
            $immeubles = Immeuble::get();
        }

        return view('home.index', compact('immeubles'));
    }
    public function show($id)
    {
       
        $immeuble = Immeuble::findOrFail($id);
       
        return view('home.show',compact('immeuble'));
    }
}

