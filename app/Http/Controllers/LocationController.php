<?php

namespace App\Http\Controllers;

use App\Models\Immeuble;
use App\Models\Location;
use App\Models\Locataire;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)

    { if($request->filled('search')){
        $locations = location::search($request->search)->simplePaginate(3);
        if ($locations->isEmpty()) {
            $message = 'Aucun résultat trouvé pour locations.';
            return view('location.index', compact('locations', 'message'));
        }
    }else{
        $locations = Location::simplePaginate(3);
    }
      
      return view('location.index' ,compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    { 
        $immeubles = Immeuble::all();
        $locataires = Locataire::all();
        return view('location.create',compact('immeubles','locataires'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
       $request->validate([
        'type_location' => 'required',
        'immeuble_id' => 'required',
        'locataire_id' => 'required',
        'debut_bail' => 'required',
        'fin_bail' => 'required|date|after:debut_bail',
        'moyen_payement' => 'required',
        'payement'=>'required',
       ]);
       Location::create($request->only(['immeuble_id','locataire_id','type_location','debut_bail','fin_bail','moyen_payement','payement']));
       return back();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Location $location)
    {
        $immeubles = Immeuble::all();
        $locataires = Locataire::all();


        return view('location.edit',compact('immeubles','locataires','location'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Location $location)
    {
        $request->validate([
            'type_location' => 'required',
        'immeuble_id' => 'required',
        'locataire_id' => 'required',
            'debut_bail' => 'required',
            'fin_bail' => 'required|date|after:debut_bail',
            'moyen_payement' => 'required',
            'payement'=>'required',
           ]);
        
        $location->fill($request->post())->save();

        return back();
    }
    /**
     * Remove the specified resource from storage.
     */
    // public function destroy(Location $location)
    // {
        
    //     $location->delete();
    //     return back();
    
    // }
    
    public function destroy(Location $location){
        $location->delete();
        $notification = [
            'message' => 'Location Deleted Sucessfully.!',
            'alert-type' => 'info'
        ];
        return back()->with($notification);
    }
}
