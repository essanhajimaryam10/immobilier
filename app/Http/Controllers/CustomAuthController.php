<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\Immeuble;
use App\Models\Location;
use App\Models\Locataire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomAuthController extends Controller
{

    public function index()
    {
        return view('auth.login');
    }  
      
    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {

            $totalLocation = Location::count();
            $totalImmeubles = Immeuble::count();
            $totalLocataires = Locataire::count();
            return view('welcome',compact('totalLocation','totalImmeubles','totalLocataires'));

        }
  
        return redirect("login")->withErrors('Login details are not valid');
    }

    public function registration()
    {
        return view('auth.register');
    }
      
    public function customRegistration(Request $request)
    {  
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
         

        ]);
           
      $data = [
        'name'=>$request->input('name'),
        'email'=>$request->input('email'),
        'password'=>Hash::make($request->input('password'))

      ];
        User::create($data);
      
        $totalLocation = Location::count();
            $totalImmeubles = Immeuble::count();
            $totalLocataires = Locataire::count();
            return view('welcome',compact('totalLocation','totalImmeubles','totalLocataires'));
    }

    public function dashboard()
    {
        
            $totalLocation = Location::count();
            $totalImmeubles = Immeuble::count();
            $totalLocataires = Locataire::count();
            return view('welcome',compact('totalLocation','totalImmeubles','totalLocataires'));
        
           
        
            // 
       
    }
    
   
    
    public function signOut() {
        Session::flush();
        Auth::logout();

        return Redirect('login');
    }
}
