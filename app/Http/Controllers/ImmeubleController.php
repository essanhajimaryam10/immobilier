<?php

namespace App\Http\Controllers;
use App\Models\Immeuble;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Redirect;
use App\Models\Immeuble\toSearchableArray;

class ImmeubleController extends Controller
{
    
    //     public function index(Request $request)
    //     {
           
           
    //         if($request->filled('search')){
    //                     $immeubles = Immeuble::search($request->search)->simplePaginate(3);
    //                 }else{
    //                     $immeubles = Immeuble::simplePaginate(3);
    //                 }

    //     return view('Immeuble.index', compact('immeubles'));
        
    // }
    public function index(Request $request)
    {
        if ($request->filled('search')) {
            $immeubles = Immeuble::search($request->search)->simplePaginate(3);
    
            if ($immeubles->isEmpty()) {
                $message = 'Aucun résultat trouvé pour immeubles.';
                return view('Immeuble.index', compact('immeubles', 'message'));
            }
        } else {
            $immeubles = Immeuble::simplePaginate(3);
        }
    
        return view('Immeuble.index', compact('immeubles'));
    }
    
    public function create()
    {
        return view('Immeuble.create');
    }
    public function store(Request $request)
    {
       $data= $request->validate([
            'type'=> 'required',
            'type_location'=> 'required',
            'addresse'=> 'required',
            'telephone'=> 'required',
            'images' => 'required|array',
            'ville'=> 'required',
            'code'=> 'required',
            'region'=> 'required',
            'surface'=> 'required',
            'etage'=> 'required',
             'prix'=> 'required',
            'description'=> 'required',
            'nbr_chambre'=> 'required',
            'info'=> 'required',
            
        ]);
        $images = [];

        foreach (  $data['images'] as $image) {
            $fileName = uniqid() . '.' . $image->getClientOriginalExtension();
            $image_path =  $image->storeAs('public/images', $fileName);

            array_push($images, $fileName);
        }
        $data['images'] = $images;
        $data['info'] = $request->input('info');
        Immeuble::create($data);
    
        return back();

    }
    public function edit(Immeuble $immeuble)
    {
        return view('Immeuble.edit',compact('immeuble'));
    }

    public function update(Request $request, Immeuble $immeuble)
    {
        $data =  $request->validate([
            'type'=> 'required',
            'type_location'=> 'required',
            'addresse'=> 'required',
            'telephone'=> 'required',
            'images' => 'required|array',
            'ville'=> 'required',
            'code'=> 'required',
            'region'=> 'required',
            'surface'=> 'required',
            'etage'=> 'required',
             'prix'=> 'required',
            'description'=> 'required',
            'nbr_chambre'=> 'required',
            'info'=> 'required',
        ]);
        $images = [];

        foreach (  $data['images'] as $image) {
            $fileName = uniqid() . '.' . $image->getClientOriginalExtension();
            $image_path =  $image->storeAs('public/images', $fileName);

            array_push($images, $fileName);
        }
        $data['images'] = $images;
        $immeuble->update($data);
        return back();
    }
   
    public function destroy(Request $request, Immeuble $immeuble)
    {
        $location = Location::where("immeuble_id", $immeuble->id)->pluck('immeuble_id');
        if ($location->isEmpty()) {
            if ($immeuble) {
                $immeuble->delete();
                session()->flash('error',"l'appartement  supprimée avec succès !");
              
            }
        } else {
            session()->flash('error', "Impossible de supprimer   parce que l'appartement est déjà utilisé");
        }
        return back();
    }
}


