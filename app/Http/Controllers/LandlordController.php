<?php

namespace App\Http\Controllers;
use App\Models\Landlord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LandlordController extends Controller
{  
    public function index()
    {

        $landlord = Landlord::simplePaginate(3);
        $user = Auth::user();
        return view('landlord.index', compact('landlord','user'));
    }
    public function create()
    {
        return view('landlord.index');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'email' => 'required',
            'ville'=> 'required',
            'region'=> 'required',
            'telephone'=> 'required',
            'codePostal'=> 'required',
            'civilite'=> 'required',
            'address' => 'required',
        ]);
     
        Landlord::create($request->all());

        return back();
    }
    public function edit(Landlord $landlord)
    {
        return view('landlord.edit',compact('landlord'));
    }
    public function update(Request $request,Landlord $landlord)
    {
        $request->validate([
            'nom' => 'required',
            'email' => 'required',
            'ville'=> 'required',
            'region'=> 'required',
            'telephone'=> 'required',
            'codePostal'=> 'required',
            'civilite'=> 'required',
            'address' => 'required',
        ]);
        
        $landlord->fill($request->post())->save();

        return back();
    }
    public function show(Landlord $landlord)
    {
        return view('landlord.show',compact('landlord'));
    }

}
